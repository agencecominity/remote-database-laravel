<?php

namespace Cominity\RemoteDatabaseLaravel\Database;

use Cominity\RemoteDatabaseLaravel\Database\Query\Processors\JsonApiProcessor;
use Cominity\RemoteDatabaseLaravel\JsonApiClient;
use Cominity\RemoteDatabaseLaravel\Database\Query\Grammars\JsonApiGrammar;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Database\Connection;
use Cominity\RemoteDatabaseLaravel\Database\Query\Builder as QueryBuilder;

class JsonApiConnection extends Connection
{

    /**
     * @var
     */
    protected $client;

    /**
     * Get a new query builder instance.
     *
     * @return Query\Builder
     */
    public function query()
    {
        return new QueryBuilder(
            $this, $this->getQueryGrammar(), $this->getPostProcessor()
        );
    }

    /**
     * @return JsonApiClient
     */
    public function client()
    {
        return new JsonApiClient($this);
    }

    /**
     * Get the default query grammar instance.
     *
     * @return JsonApiGrammar
     */
    protected function getDefaultQueryGrammar()
    {
        return new JsonApiGrammar();
    }

    /**
     * Get the default post processor instance.
     *
     * @return JsonApiProcessor
     */
    protected function getDefaultPostProcessor()
    {
        return new JsonApiProcessor();
    }

    /**
     * Run an insert statement against the database.
     *
     * @param  string  $query
     * @param  array  $bindings
     * @return array
     */
    public function insert($query, $bindings = [])
    {
        return $this
            ->client()
            ->create($query, $bindings['data'] ?? '');
    }

    /**
     * Run an update statement against the database.
     *
     * @param  string  $query
     * @param  array  $bindings
     * @return int
     */
    public function update($query, $bindings = [])
    {
        return $this
            ->client()
            ->update($query, $bindings['id'], $bindings['data'] ?? []);
    }

    /**
     * Run an update statement against the database.
     *
     * @param string $endpoint
     * @param array $where
     * @param string $function
     * @param array $columns
     * @return int
     * @throws GuzzleException
     */
    public function aggregate(
        string $endpoint,
        array $where,
        string $function,
        array $columns,
    )
    {
        return $this
            ->client()
            ->aggregate($endpoint, $where, $function, $columns);
    }

}
