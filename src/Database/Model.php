<?php

namespace Cominity\RemoteDatabaseLaravel\Database;


use Illuminate\Database\Eloquent\Builder;
use Cominity\RemoteDatabaseLaravel\Database\Builder as JsonApiBuilder;

abstract class Model extends \Illuminate\Database\Eloquent\Model
{

    /**
     * Save the model to the database.
     *
     * @param  array  $options
     * @return bool
     */
    public function save(array $options = [])
    {
        $this->mergeAttributesFromClassCasts();

        $query = $this->newModelQuery();

        // If the "saving" event returns false we'll bail out of the save and return
        // false, indicating that the save failed. This provides a chance for any
        // listeners to cancel save operations if validations fail or whatever.
        if ($this->fireModelEvent('saving') === false) {
            return false;
        }

        // If the model already exists in the database we can just update our record
        // that is already in this database using the current IDs in this "where"
        // clause to only update this model. Otherwise, we'll just insert them.
        if ($this->exists) {
            $saved = $this->isDirty() ?
                $this->performUpdate($query) : true;
        }

        // If the model is brand new, we'll insert it into our database and set the
        // ID attribute on the model to the value of the newly inserted row's ID
        // which is typically an auto-increment value managed by the database.
        else {
            $saved = $this->performInsert($query);
            if (! $this->getConnectionName() &&
                $connection = $query->getConnection()) {
                $this->setConnection($connection->getName());
            }
        }

        // If the model is successfully saved, we need to do a few more things once
        // that is done. We will call the "saved" method here to run any actions
        // we need to happen after a model gets successfully saved right here.
        if ($saved) {
            $this->finishSave($options);
        }

        return $saved;
    }

    /**
     * Perform a model insert operation.
     *
     * @param Builder|\Cominity\RemoteDatabaseLaravel\Database\Builder $query
     * @return bool
     */
    protected function performInsert(
        Builder|JsonApiBuilder $query
    )
    {
        if ($this->fireModelEvent('creating') === false) {
            return false;
        }

        // First we'll need to create a fresh query instance and touch the creation and
        // update timestamps on this model, which are maintained by us for developer
        // convenience. After, we will just continue saving these model instances.
//        if ($this->usesTimestamps()) {
//            $this->updateTimestamps();
//        }

        // If the model has an incrementing key, we can use the "insertGetId" method on
        // the query builder, which will give us back the final inserted ID for this
        // table from the database. Not all tables have to be incrementing though.
        $attributes = $this->getAttributesForInsert();

        if ($this->getIncrementing()) {
            $this->insertAndSetId($query, $attributes);
        }

        // If the table isn't incrementing we'll simply insert these attributes as they
        // are. These attribute arrays must contain an "id" column previously placed
        // there by the developer as the manually determined key for these models.
        else {
            if (empty($attributes)) {
                return true;
            }

            $query->insert($attributes);
        }

        // We will go ahead and set the exists property to true, so that it is set when
        // the created event is fired, just in case the developer tries to update it
        // during the event. This will allow them to do so and run an update here.
        $this->exists = true;

        $this->wasRecentlyCreated = true;

        $this->fireModelEvent('created', false);

        return true;
    }

    /**
     * Insert the given attributes and set the ID on the model.
     *
     * @param Builder|\Cominity\RemoteDatabaseLaravel\Database\Builder $query
     * @param array $attributes
     * @return void
     */
    protected function insertAndSetId(
        JsonApiBuilder|Builder $query,
        $attributes
    )
    {
        $newAttributes = $query->insertGetId($attributes);//TODO fix this. new method returned Builder

//        $keyName = $this->getKeyName();
//
//        if(isset($newAttributes[$keyName])) {
//            $this->setAttribute($keyName, $newAttributes[$keyName]);
//        }

        foreach ($newAttributes as $field => $value){
            $this->setAttribute($field, $value);
        }

    }

}
