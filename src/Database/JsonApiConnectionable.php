<?php

namespace Cominity\RemoteDatabaseLaravel\Database;

use Cominity\RemoteDatabaseLaravel\Database\Builder as JsonApiBuilder;

trait JsonApiConnectionable
{

    /**
     * Disable timestamps for api calls
     *
     * @var bool
     */
    //public $timestamps = false;

    /**
     * Create a new Eloquent query builder for the model.
     *
     * @param  \Illuminate\Database\Query\Builder  $query
     * @return Builder
     */
    public function newEloquentBuilder($query)
    {
        return new JsonApiBuilder($query);
    }

    /**
     * Get a new query builder that doesn't have any global scopes or eager loading.
     *
     * @return JsonApiBuilder
     */
    public function newModelQuery()
    {
        return $this->newEloquentBuilder(
            $this->newBaseQueryBuilder()
        )->setModel($this);
    }

//    /**
//     * Get a new query builder instance for the connection.
//     *
//     * @return \Illuminate\Database\Query\Builder
//     */
//    protected function newBaseQueryBuilder()
//    {
//        dd($this->getConnection());
//        return $this->getConnection()->query();
//    }

//    /**
//     * Get the database connection for the model.
//     *
//     * @return \Illuminate\Database\Connection
//     */
//    public function getConnection()
//    {
//        dd($this->getConnectionName());
//        return static::resolveConnection($this->getConnectionName());
//    }

//    /**
//     * @return string|null
//     */
//    public function getConnectionName(): ?string
//    {
//        dd($this->connection);
//        return $this->connection;
//    }

}
