<?php

namespace Cominity\RemoteDatabaseLaravel\Database\Query;

use Cominity\RemoteDatabaseLaravel\Database\Query\Grammars\JsonApiGrammar;
use Cominity\RemoteDatabaseLaravel\Database\Query\Processors\JsonApiProcessor;
use Illuminate\Database\ConnectionInterface;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Arr;

class Builder extends \Illuminate\Database\Query\Builder
{

    /**
     * Create a new query builder instance.
     *
     * @param ConnectionInterface $connection
     * @param JsonApiGrammar|null $grammar
     * @param JsonApiProcessor|null $processor
     */
    public function __construct(ConnectionInterface $connection,
                                JsonApiGrammar $grammar = null,
                                JsonApiProcessor $processor = null)
    {
        $this->connection = $connection;
        $this->grammar = $grammar ?: $connection->getQueryGrammar();
        $this->processor = $processor ?: $connection->getPostProcessor();
    }

    /**
     * Execute the query as a "select" statement.
     *
     * @param  array|string  $columns
     * @return \Illuminate\Support\Collection
     */
    public function get($columns = ['*'])
    {
        $wheres = $this->getWheres($this->wheres);
        $joins = $this->getJoins($this->joins);

        return collect($this->onceWithColumns(Arr::wrap($columns), function () use ($wheres, $joins){
            return $this
                ->connection
                ->client()
                ->limit($this->limit)
                ->offset($this->offset)
                ->get($this->from, $wheres, $joins, $this->columns);
        }));
    }

    /**
     * @param $wheres
     * @return array
     */
    protected function getWheres(
        $wheres
    ): array
    {
        $data = [];
        foreach($wheres as $where){
            if(isset($where['type']) && $where['type'] === 'Nested'){
                $newWheres = $this->getWheres($where['query']->wheres);
                $data = array_merge($newWheres, $data);
            }else{
                $data[] = $where;
            }
        }
        return $data;
    }

    /**
     * @param $joins
     * @return array
     */
    protected function getJoins(
        $joins
    ): array
    {
        if($this->joins === null) {
            return [];
        }
        $data = [];
        foreach($joins as $join){
            $data[] = [
                'table' => $join->table,
                'type' => $join->type,
                'wheres' => $this->getWheres($join->wheres),
            ];
        }
        return $data;
    }

    /**
     * Update records in the database.
     *
     * @param  array  $values
     * @return int
     */
    public function update(array $values)
    {
        $this->applyBeforeQueryCallbacks();
        $id = '';
        foreach($this->wheres as $where){
            if(isset($where['column']) && $where['column'] === 'id' && isset($where['value'])){
                $id = $where['value'];
            }
        }
        if(!$id){
            throw new \Exception('no ID');
        }
        return $this->connection->update($this->from, [
            'id' => $id,
            'data' => $values,
        ]);
    }

    /**
     * Execute an aggregate function on the database.
     *
     * @param  string  $function
     * @param  array  $columns
     * @return mixed
     */
    public function aggregate($function, $columns = ['*'])
    {
        $wheres = $this->getWheres($this->wheres);
        return $this->connection->aggregate(
            $this->from,
            $wheres,
            $function,
            $columns
        );
    }

    /**
     * Paginate the given query into a simple paginator.
     *
     * @param  int  $perPage
     * @param  array  $columns
     * @param  string  $pageName
     * @param  int|null  $page
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginate($perPage = 15, $columns = ['*'], $pageName = 'page', $page = null)
    {
        //TODO check
        $page = $page ?: Paginator::resolveCurrentPage($pageName);

        $total = $this->getCountForPagination();

        $results = $total ? $this->forPage($page, $perPage)->get($columns) : collect();

        return $this->paginator($results, $total, $perPage, $page, [
            'path' => Paginator::resolveCurrentPath(),
            'pageName' => $pageName,
        ]);
    }

    /**
     * Get the count of the total records for the paginator.
     *
     * @param  array  $columns
     * @return int
     */
    public function getCountForPagination($columns = ['*'])
    {
        $wheres = $this->getWheres($this->wheres);
        return $this->connection->aggregate(
            $this->from,
            $wheres,
            'count',
            $columns
        );
    }

    /**
     * Get a new join clause.
     *
     * @param  \Illuminate\Database\Query\Builder  $parentQuery
     * @param  string  $type
     * @param  string  $table
     * @return \Illuminate\Database\Query\JoinClause
     */
    protected function newJoinClause($parentQuery, $type, $table)
    {
        return new JoinClause($parentQuery, $type, $table);
    }

}
