<?php

namespace Cominity\RemoteDatabaseLaravel;

use Cominity\RemoteDatabaseLaravel\Database\JsonApiConnection;
use Cominity\RemoteDatabaseLaravel\Database\Query\Builder as QueryBuilder;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Str;

class JsonApiClient
{

    /**
     * @var Client
     */
    protected Client $client;

    /**
     * @var JsonApiConnection
     */
    protected JsonApiConnection $jsonApiConnection;

    /**
     * @var null|int
     */
    protected ?int $limit = null;

    /**
     * @var null|int
     */
    protected ?int $offset = null;

    /**
     * @param int $limit
     */
    public function limit(?int $limit){
        $this->limit = $limit;
        return $this;
    }

    /**
     * @param int $offset
     */
    public function offset(?int $offset){
        $this->offset = $offset;
        return $this;
    }

    /**
     * @param QueryBuilder $query
     * @param array $config
     */
    public function __construct(
        JsonApiConnection $jsonApiConnection,
    )
    {
        $this->jsonApiConnection = $jsonApiConnection;
        $this->client = new Client([
            'base_uri' => $jsonApiConnection->getConfig('api_url'),
            'headers' => $jsonApiConnection->getConfig('headers') + [
                'Content-Type' => 'application/vnd.api+json',
            ]
        ]);
    }

    /**
     * @return mixed
     * @throws GuzzleException
     */
    public function get(
        string $endpoint,
        array $where,
        array $joins = null,
        array $columns = null,
    ): mixed
    {
//        dd($where);
        $data = [
            'filter' => $where,
        ];
        if($this->limit !== null){
            $data['limit'] = $this->limit;
        }
        if($joins !== null){
            $data['joins'] = $joins;
        }
        if($columns !== null){
            $data['select'] = $columns;
        }
        if($this->offset !== null){
            $data['offset'] = $this->offset;
        }
        $response = $this->client->get($this->prepareEndpoint($endpoint), [
            'query' => $data
        ]);
//        dd($response->getBody()->getContents());
        return json_decode($response->getBody()->getContents());
    }

    /**
     * @param string $attributes
     * @return array
     * @throws GuzzleException
     */
    public function create(
        string $endpoint,
        array $attributes,
    ): array
    {
        $response = $this->client->post($this->prepareEndpoint($endpoint), [
            'json' => $attributes
        ]);
        return json_decode($response->getBody()->getContents(), 1);
    }

    /**
     * @param string|int $id
     * @param string $attributes
     * @return array
     * @throws GuzzleException
     */
    public function update(
        string $endpoint,
        string|int $id,
        array $attributes
    ): array
    {
        $response = $this->client->put($this->prepareEndpoint($endpoint) . '/' . $id, [
            'json' => $attributes
        ]);
        return json_decode($response->getBody()->getContents(), 1);
    }

    /**
     * @param string $endpoint
     * @param array $where
     * @param string $function
     * @param array $columns
     * @return mixed
     * @throws GuzzleException
     */
    public function aggregate(
        string $endpoint,
        array $where,
        string $function,
        array $columns,
    ): mixed
    {
        //dd($where);
        $response = $this->client->get($this->prepareEndpoint($endpoint), [
            'query' => [
                'filter' => $where,
                'response_type' => $function,
            ]
        ]);
//        dd($response->getBody()->getContents());
        return $response->getBody()->getContents() ?? 0;
    }

    /**
     * convert table name to endpoint
     *
     * @param string $endpoint
     * @return string
     */
    protected function prepareEndpoint(
        string $endpoint
    ): string
    {
        return Str::replace('_', '-', $endpoint);
    }

}
