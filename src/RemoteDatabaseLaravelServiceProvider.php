<?php

namespace Cominity\RemoteDatabaseLaravel;

use Cominity\RemoteDatabaseLaravel\Database\JsonApiConnection;
use Illuminate\Database\Connection;
use Illuminate\Support\ServiceProvider;

class RemoteDatabaseLaravelServiceProvider extends ServiceProvider
{

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        Connection::resolverFor(
            'json_api',
            fn($connection, $database, $prefix, $config) => app()->has(JsonApiConnection::class)
                ? app(JsonApiConnection::class)
                : new JsonApiConnection($connection, $database, $prefix, $config)
            );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
